extends "SavableNode.gd"

const ComputerPartInStock = preload("res://ComputerPartInStock.tscn")
const PartModel = preload("res://ComputerPartModel.gd")
const Customer = preload("res://Customer.gd")
const Employee = preload("res://Employee.gd")

var money = 1000
signal money_changed

enum MoneyTransfer {
	BUY_CASE = PartModel.Type.CASE,
	BUY_MOTHERBOARD = PartModel.Type.MOTHERBOARD,
	BUY_CPU = PartModel.Type.CPU,
	BUY_GRAPHICBOARD = PartModel.Type.GRAPHICBOARD,
	BUY_SCREEN = PartModel.Type.SCREEN,
	BUY_KEYBOARD = PartModel.Type.KEYBOARD,
	SALARY_BUYER,
	SALARY_SELLER,
	SEVERANCE,
	SELL_COMPUTER,
}
var money_transfers = []

var day = 0
signal day_changed

enum Step {BUYS, SELL, EMPLOYEES, BALANCE}
var step = 0
signal step_changed

# const data, generated at game initialization
var weighed_customers_profiles = []
var customer_max_nb_computers = {}
var average_specs_costs = {}
var max_possible_computer_specs = {}


func _ready():
	_make_weighed_customers_profiles()
	_make_customer_max_nb_computers()
	_make_max_possible_specs()
	_make_average_specs_costs()
	
	# Generate some stock, useful for tests
#	for part_model in $AllPartModels.get_children():
#		var part_in_stock = ComputerPartInStock.instance()
#		part_in_stock.model = part_model
#		add_to_stock(part_in_stock, (randi() % 5) + 1)

func _update_step_gui():
	var children = $VBoxContainer/StepGuiContainer.get_children()
	if !children.empty():
		$VBoxContainer/StepGuiContainer.remove_child(children[0])
	
	var step_gui = null
	match step:
		Step.BUYS:
			step_gui = preload("res://GuiStepBuy.tscn").instance()
		Step.SELL:
			step_gui = preload("res://GuiStepSell.tscn").instance()
		Step.EMPLOYEES:
			step_gui = preload("res://GuiStepEmployees.tscn").instance()
		Step.BALANCE:
			step_gui = preload("res://GuiStepBalance.tscn").instance()
	
	$VBoxContainer/StepGuiContainer.add_child(step_gui)

func _set_money_delta(delta, reason):
	if delta != 0:
		money += delta
		money_transfers[day - 1][reason] += delta
		emit_signal("money_changed")
		save_game()

func get_money_label(value=null):
	if value == null:
		value = money
	
	return "%d §" % value

func add_to_stock(part_in_provider_stock, count=1):
	var part_in_stock = null
	for actual_part_in_stock in $Stock.get_children():
		if actual_part_in_stock.model == part_in_provider_stock.model:
			part_in_stock = actual_part_in_stock
			break
	
	if part_in_stock == null:
		part_in_stock = ComputerPartInStock.instance()
		part_in_stock.model = part_in_provider_stock.model
		$Stock.add_child(part_in_stock)
	
	part_in_stock.count += count
	part_in_provider_stock.count -= count
	
	_set_money_delta(-part_in_provider_stock.price * count, 
					 part_in_provider_stock.model.type)

func next_step():
	step += 1
	
	if step >= len(Step):
		step = 0
	
	if step == Step.EMPLOYEES:
		_end_employees_day()
	
	if step == 0:
		prepare_new_day()
	
	_update_step_gui()
	
	emit_signal("step_changed")
	
	save_game()

func _end_employees_day():
	for employee in $Employees.get_children():
		if not Employee.CAPACITY_MANAGER in employee.capacities:
			var reason = -1
			if Employee.CAPACITY_BUY in employee.capacities:
				reason = MoneyTransfer.SALARY_BUYER
			elif Employee.CAPACITY_SELL in employee.capacities:
				reason = MoneyTransfer.SALARY_SELLER
		
			_set_money_delta(-get_employee_salary(employee.level), reason)
		
		employee.experience += 50
		employee.level = get_employee_level(employee.experience)

func prepare_new_day():
	var total_daily_buys = 0
	var total_daily_sells = 0
	
	for employee in $Employees.get_children():
		employee.daily_buys = get_daily_buys(employee)
		total_daily_buys += employee.daily_buys
		
		employee.daily_sells = get_daily_sells(employee)
		total_daily_sells += employee.daily_sells
	
	# Make a bit more daily providers than available calls so that the player may choose
	var nb_daily_providers = int(round(total_daily_buys * rand_range(1.0, 1.5)))
	nb_daily_providers = max(nb_daily_providers, 3)
	
	var daily_providers = $AllProviders.get_children()
	while daily_providers.size() > nb_daily_providers:
		daily_providers.remove(randi() % daily_providers.size())

	Utils.clear_children($DailyProviders)

	for provider in daily_providers:
		var copy = provider.duplicate()
		$DailyProviders.add_child(copy)
		_make_provider_daily_stock(copy)
	
	# Make a bit more daily customers than available calls so that the player may choose
	Utils.clear_random($DailyCustomers, $DailyCustomers.get_child_count() / 2)

	var nb_daily_customers = int(round(total_daily_sells * rand_range(1.3, 1.8)))
	nb_daily_customers = max(3, nb_daily_customers)
	while $DailyCustomers.get_child_count() < nb_daily_customers:
		_make_daily_customer()
	
	# Make some candidates for employment
	Utils.clear_random($DailyCandidates, $DailyCandidates.get_child_count() / 2)
	
	var nb_daily_candidates = int(round(rand_range(2, 8)))
	while $DailyCandidates.get_child_count() < nb_daily_candidates:
		_make_daily_candidate()
	
	day += 1
	emit_signal("day_changed")
	
	# Prepare money stats
	var daily_money_transfers = []
	for transfer in MoneyTransfer.values():
		daily_money_transfers.append(0)
	money_transfers.append(daily_money_transfers)

func get_employee_salary(level):
	return level * 15

func get_employee_level(experience):
	return int(experience / 100)

func get_daily_buys(employee):
	if employee.CAPACITY_BUY in employee.capacities:
		return int(employee.level / 6) + 1
	else:
		return 0

func get_daily_sells(employee):
	if employee.CAPACITY_SELL in employee.capacities:
		return int(employee.level / 2) + 2
	else:
		return 0

func evaluate_customer_offer(customer, seller, computer, price):
	seller.take_daily_sell()
	
	var divider_score_exact_more = 5.0
	
	var score_specs = 0.0
	var nb_watched_specs = 0
	for spec in PartModel.Spec.values():
		var expected_spec = customer.expected_specs[spec]
		if expected_spec[0] != Customer.SpecMatch.NONE:
			nb_watched_specs += 1
			
			var delta = computer.get_spec(spec) - expected_spec[1]
			
			print(computer.get_spec(spec), " = ", expected_spec[1])
			
			if (expected_spec[0] == Customer.SpecMatch.EXACT &&
				delta > 0):
				delta /= divider_score_exact_more
				
			print(spec, " ", delta)
			score_specs += delta
	
	score_specs /= nb_watched_specs
	score_specs /= 50.0
	 
	var specs_accepted = false
	if score_specs > 0.0:
		specs_accepted = true
	else:
		specs_accepted = abs(score_specs) < customer.specs_score_tolerancy
	
	var score_price = customer.budget - price
	score_price /= 200.0
	
	var price_accepted = false
	if score_price > 0.0:
		price_accepted = true
	else:
		price_accepted = abs(score_price) < customer.budget_score_tolerancy
	
	print("score specs = ", score_specs, " score price = ", score_price)
	
	if specs_accepted && price_accepted:
		# Get money from customer
		_set_money_delta(price * customer.nb_computers, MoneyTransfer.SELL_COMPUTER)
		
		# Take parts out to make computers
		var parts_to_remove = []
		for part in computer.parts:
			for part_in_stock in get_tree().get_nodes_in_group("stock"):
				if part_in_stock.model == part:
					part_in_stock.count -= customer.nb_computers
					if part_in_stock.count <= 0:
						parts_to_remove.append(part_in_stock)
		
		for part_to_remove in parts_to_remove:
			remove_child(part_to_remove)
		
		# Remove customer from list
		$DailyCustomers.remove_child(customer)
	
	return [specs_accepted, price_accepted]

func hire_candidate(candidate):
	$DailyCandidates.remove_child(candidate)
	$Employees.add_child(candidate)
	_connect_employee(candidate)
	save_game()

func fire_employee(employee):
	$Employees.remove_child(employee)
	_set_money_delta(-get_employee_salary(employee.level), MoneyTransfer.SEVERANCE)

func add_children_to_save(parent_node):
	var list = [parent_node]
	
	for child in parent_node.children_to_save():
		list += add_children_to_save(child)
	
	return list

static func has_saved_game():
	return File.new().file_exists("user://game.save")

static func get_random_first_name_man():
	return Utils.get_random(first_names_men())

static func get_random_first_name_woman():
	return Utils.get_random(first_names_women())

static func get_random_first_name():
	return Utils.get_random(first_names_men() + first_names_women())

static func get_random_last_name():
	return Utils.get_random(last_names())

func prepare_new(manager_first_name, manager_last_name):
	var manager = preload("res://Employee.tscn").instance()
	_make_character(manager)
	manager.first_name = manager_first_name
	manager.last_name = manager_last_name
	manager.experience = 0
	manager.level = get_employee_level(manager.experience)
	manager.capacities = [Employee.CAPACITY_BUY, Employee.CAPACITY_SELL, Employee.CAPACITY_MANAGER]
	$Employees.add_child(manager)
	_connect_employee(manager)
	
	prepare_new_day()
	_update_step_gui()
	save_game()

static func load(root : Node):
	var save_file = File.new()
	save_file.open("user://game.save", File.READ)
	
	var game = null
	var referenced_nodes = []
	
	while not save_file.eof_reached():
		var current_line = parse_json(save_file.get_line())
		if current_line != null:
			var new_object = load(current_line["filename"]).instance()
			
			if new_object.filename == "res://Game.tscn":
				game = new_object
			
			new_object.name = current_line["name"]
			
			for key in current_line.keys():
				if key != "filename" and key != "parent" and key != "name":
					if key.begins_with("node_"):
						referenced_nodes.append([new_object, key, current_line[key]])
					elif key.begins_with("int_"):
						new_object.set(key.right(4), int(current_line[key]))
					elif key.begins_with("color_"):
						var array = current_line[key]
						var color = Color(array[0], array[1], array[2], array[3])
						new_object.set(key.right(6), color)
					else:
						new_object.set(key, current_line[key])
			
			root.get_node(current_line["parent"]).add_child(new_object)
	
	for referenced_node in referenced_nodes:
		var object = referenced_node[0]
		var key = referenced_node[1]
		var value = referenced_node[2]
		
		object.set(key.right(5), root.get_node(value))
	
	save_file.close()
	
	for employee in game.get_node("Employees").get_children():
		game._connect_employee(employee)
	
	game._update_step_gui()
	return game

func save_game():
	var save_file = File.new()
	save_file.open("user://game.save", File.WRITE)
	
	for node in add_children_to_save(self):
		var dict = { 
			"filename" : node.get_filename(),
			"parent" : node.get_parent().get_path(),
			"name" : node.name
		}
		
		for property in node.properties_to_save():
			var value = node.get(property)
			
			if value is Node:
				value = value.get_path()
				property = "node_%s" % property
			elif value is int:
				property = "int_%s" % property
			elif value is Color:
				value = [value.r, value.g, value.b, value.a]
				property = "color_%s" % property
			
			dict[property] = value
			
		save_file.store_line(to_json(dict))
	
	save_file.close()

func children_to_save():
	return (.children_to_save() + 
			$Employees.get_children() +
		    $Stock.get_children() +
			$DailyProviders.get_children() +
			$DailyCandidates.get_children() +
			$DailyCustomers.get_children())

func properties_to_save():
	return .properties_to_save() + ["money", "money_transfers", "day", "step"]

func _connect_employee(employee):
	employee.connect("daily_buys_changed", self, "save_game")
	employee.connect("daily_sells_changed", self, "save_game")

func _make_provider_daily_stock(provider):
	var nb_parts = int(round($AllPartModels.get_child_count() * rand_range(0.1, 0.3)))
	var stock = $AllPartModels.get_children()
	while stock.size() > nb_parts:
		stock.remove(randi() % stock.size())
	
	var parts_in_stock = []
	
	for part_model in stock:
		var part_in_stock = ComputerPartInStock.instance()
		part_in_stock.model = part_model
		part_in_stock.price = int(round(rand_range(part_model.min_cost, part_model.max_cost)))
		part_in_stock.count = (randi() % 50) + 1
		parts_in_stock.append(part_in_stock)
	
	provider.set_daily_stock(parts_in_stock)

func _make_customer_expected_specs(customer):
	var expected_computing = []
	var expected_graphics = []
	var expected_ergonomy = []
	var max_computing = max_possible_computer_specs[PartModel.Spec.COMPUTING]
	var max_graphics = max_possible_computer_specs[PartModel.Spec.GRAPHICS]
	var max_ergonomy = max_possible_computer_specs[PartModel.Spec.ERGONOMY]
	
	match customer.profile:
		Customer.Profile.PRIVATE, Customer.Profile.COMPANY:
			expected_computing = [Customer.SpecMatch.EXACT, 
			                      rand_range(0.2, 0.6) * max_computing]
			expected_graphics = [Customer.SpecMatch.NONE]
			expected_ergonomy = [Customer.SpecMatch.EXACT, 
			                     rand_range(0.2, 0.6) * max_ergonomy]
		Customer.Profile.DEVELOPPER, Customer.Profile.CONSULTING_COMPANY:
			expected_computing = [Customer.SpecMatch.ATLEAST, 
			                      rand_range(0.5, 1.0) * max_computing]
			expected_graphics = [Customer.SpecMatch.NONE]
			expected_ergonomy = [Customer.SpecMatch.EXACT, 
			                     rand_range(0.2, 0.8) * max_ergonomy]
		Customer.Profile.GAMER, Customer.Profile.LAN_GAMING_CENTER:
			expected_computing = [Customer.SpecMatch.ATLEAST, 
			                      rand_range(0.5, 1.0) * max_computing]
			expected_graphics = [Customer.SpecMatch.ATLEAST, 
			                     rand_range(0.5, 1.0) * max_graphics]
			expected_ergonomy = [Customer.SpecMatch.ATLEAST, 
			                     rand_range(0.5, 1.0) * max_ergonomy]
		Customer.Profile.DESIGNER, Customer.Profile.DESIGN_STUDIO:
			expected_computing = [Customer.SpecMatch.NONE]
			expected_graphics = [Customer.SpecMatch.ATLEAST, 
			                     rand_range(0.7, 1.0) * max_graphics]
			expected_ergonomy = [Customer.SpecMatch.ATLEAST, 
			                     rand_range(0.5, 1.0) * max_ergonomy]
	
	customer.expected_specs[PartModel.Spec.COMPUTING] = expected_computing
	customer.expected_specs[PartModel.Spec.GRAPHICS] = expected_graphics
	customer.expected_specs[PartModel.Spec.ERGONOMY] = expected_ergonomy
	
	for expected_spec in customer.expected_specs:
		var spec_value = customer.expected_specs[expected_spec]
		if len(spec_value) > 1:
			spec_value[1] = Utils.round(spec_value[1], 10)

func _make_customer_budget(customer):
	customer.budget = 0.0
	
	for spec in PartModel.Spec.values():
		var expected_spec = customer.expected_specs[spec]
		if expected_spec[0] != Customer.SpecMatch.NONE:
			customer.budget += average_specs_costs[spec] * expected_spec[1]
	
	customer.budget *= rand_range(0.8, 1.6)
	customer.budget = Utils.round(customer.budget, 50)

func _make_daily_customer():
	var customer = preload("res://Customer.tscn").instance()
	
	_make_character(customer)
	customer.profile = weighed_customers_profiles[randi() % len(weighed_customers_profiles)]
	customer.nb_computers = (randi() % customer_max_nb_computers[customer.profile]) + 1
	customer.specs_score_tolerancy = rand_range(0.2, 0.5)
	customer.budget_score_tolerancy = rand_range(0.2, 0.5)
	_make_customer_expected_specs(customer)
	_make_customer_budget(customer)

	$DailyCustomers.add_child(customer)

func _make_daily_candidate():
	var candidate = preload("res://Employee.tscn").instance()
	
	_make_character(candidate)
	if randi() % 2 == 0:
		candidate.capacities.append(Employee.CAPACITY_BUY)
	else:
		candidate.capacities.append(Employee.CAPACITY_SELL)

	candidate.experience = int(pow(rand_range(0, 1), 2) * 1500)
	candidate.level = get_employee_level(candidate.experience)
	
	$DailyCandidates.add_child(candidate)

func _make_character(character):
	character.gender = randi() % len(character.Gender)
	
	if character.gender == character.Gender.WOMAN:
		character.first_name = get_random_first_name_woman()
	else:
		character.first_name = get_random_first_name_man()
	
	character.color_hair = Color(rand_range(0.0, 1.0),
								 rand_range(0.0, 1.0),
								 rand_range(0.0, 1.0),
								 1.0)
	character.color_clothe = Color(rand_range(0.0, 1.0),
								   rand_range(0.0, 1.0),
								   rand_range(0.0, 1.0),
								   1.0)
	
	var gradient = Gradient.new()
	gradient.set_color(0, Color(1.0, 0.914, 0.710))
	gradient.add_point(0.4, Color(1.0, 0.839, 0.451))
	gradient.set_color(1, Color(0.27, 0.193, 0.104))
	character.color_skin = gradient.interpolate(rand_range(0.0, 1.0))
	
	character.last_name = get_random_last_name()

func _make_weighed_customers_profiles():
	var weighed_profiles = {
		Customer.Profile.GAMER: 3,
		Customer.Profile.LAN_GAMING_CENTER: 1,
		Customer.Profile.PRIVATE: 6,
		Customer.Profile.COMPANY: 5,
		Customer.Profile.DEVELOPPER: 2,
		Customer.Profile.CONSULTING_COMPANY: 3,
		Customer.Profile.DESIGNER: 1,
		Customer.Profile.DESIGN_STUDIO: 2
	}
	
	for profile in weighed_profiles:
		for i in range(weighed_profiles[profile]):
			weighed_customers_profiles.append(profile)

func _make_customer_max_nb_computers():
	customer_max_nb_computers = {
		Customer.Profile.GAMER: 1,
		Customer.Profile.LAN_GAMING_CENTER: 10,
		Customer.Profile.PRIVATE: 1,
		Customer.Profile.COMPANY: 15,
		Customer.Profile.DEVELOPPER: 1,
		Customer.Profile.CONSULTING_COMPANY: 15,
		Customer.Profile.DESIGNER: 1,
		Customer.Profile.DESIGN_STUDIO: 5
	}

func _make_max_possible_specs():
	for spec in PartModel.Spec.values():
		var total = 0
		for type in PartModel.Type.values():
			var max_spec = 0
			for part_model in $AllPartModels.get_children():
				if part_model.type == type:
					max_spec = max(max_spec, part_model.get_spec(spec))
			total += max_spec
		max_possible_computer_specs[spec] = total

func _make_average_specs_costs():
	for spec in PartModel.Spec.values():
		var total_cost = 0.0
		var total_spec = 0
		for part in $AllPartModels.get_children():
			var part_total_specs = 0.0
			for part_spec in PartModel.Spec.values():
				part_total_specs += part.get_spec(part_spec)
			
			var spec_proportion = part.get_spec(spec) / part_total_specs
			total_spec += part.get_spec(spec)
			total_cost += ((part.min_cost + part.max_cost) / 2.0) * spec_proportion
		average_specs_costs[spec] = total_cost / total_spec

static func first_names_men():
	return [
		"Alain", 
		"Albert", 
		"Alexis",
		"André", 
		"Barry",
		"Charles", 
		"Christian", 
		"Clark", 
		"Damien", 
		"David", 
		"Elouan", 
		"Enzo", 
		"Erwan", 
		"Ethan", 
		"Ewen", 
		"Félix",
		"François", 
		"George", 
		"Gildas", 
		"Gilles", 
		"Gorka", 
		"Guilhem", 
		"Harry", 
		"Hugo", 
		"Indiana", 
		"James", 
		"Jean-Louis", 
		"John", 
		"Joseph",
		"Jules", 
		"Julien", 
		"Korben", 
		"Laurent", 
		"Léon", 
		"Léonis", 
		"Matthieu", 
		"Maxence",
		"Mathys",
		"Maxime",
		"Melaine", 
		"Michael", 
		"Michel", 
		"Mike", 
		"Nicolas", 
		"Patrick", 
		"Paul", 
		"Philippe", 
		"Richard", 
		"Robert", 
		"Roger",
		"Romain",
		"Ronan", 
		"Sébastien", 
		"Sylvain", 
		"Timéo",
		"Titouan", 
		"Vincent",
		"Yann"]

static func first_names_women():
	return [
		"Anne",
		"Anne-Claire",
		"Audrey",
		"Camille",
		"Capucine",
		"Caroline",
		"Cécilia",
		"Charlène",
		"Chloé",
		"Christiane",
		"Claire",
		"Clélie",
		"Éléa",
		"Elizabeth",
		"Emeline",
		"Émilie",
		"Emma",
		"Fanny",
		"Floriane",
		"Gaëlle",
		"Georgia",
		"Hanaé",
		"Isabelle",
		"Janet",
		"Julie",
		"Juliette",
		"Inès",
		"Lali",
		"Lily",
		"Lisa",
		"Lise",
		"Lois",
		"Lola",
		"Louise",
		"Ludivine",
		"Magali",
		"Manon",
		"Marie",
		"Marina",
		"Marine",
		"Marion",
		"Michelle",
		"Nadine",
		"Nina",
		"Rejwan",
		"Régine",
		"Sandrine",
		"Sarah",
		"Sharon",
		"Soazig",
		"Solène",
		"Soline",
		"Sophie",
		"Susan",
		"Sylvie",
		"Tiphaine",
		"Véronique",
		"Wendy",
		"Yanne"]

static func last_names():
	return [
		"Abdayem",
		"Anderson",
		"Barlet",
		"Barusseau",
		"Beaulieu",
		"Black",
		"Bouquet",
		"Cardou",
		"Carquin",
		"Chatellier",
		"Charbonnel",
		"Courcoux",
		"Droguet",
		"Dupont",
		"Floc'h",
		"Halgand",
		"Jackson",
		"Jones",
		"Kent",
		"King",
		"Laisné",
		"Lane",
		"Lee",
		"Linet",
		"Mabit",
		"Marbais",
		"Martin",
		"Mathieu",
		"Miller",
		"Moore",
		"Moua",
		"Obama",
		"Orveillon",
		"Panhelleux",
		"Pierquin",
		"Potter",
		"Pousse",
		"Prime",
		"Rivault",
		"Smith",
		"Struillou",
		"Taugain",
		"Taylor",
		"Thompson",
		"Verne",
		"Weasley",
		"White",
		"Williams",
		"Wilson",
		"Wright",
		"Wittemberg"]
