extends Control

const EmployeeItem = preload("res://ManageEmployeeItem.tscn")
const CustomerItem = preload("res://CustomerItem.tscn")

onready var game = get_node("/root/Main/Game")


func _ready():
	for employee in game.get_node("Employees").get_children():
		if not employee.CAPACITY_MANAGER in employee.capacities:
			_add_employee(employee)
		
	$ContainerSelect/HBoxContainer/Employees/ContainerEmployees.update()

	for candidate in game.get_node("DailyCandidates").get_children():
		var item = _make_employee_item(candidate, 
									   $ContainerSelect/HBoxContainer/Candidates/ContainerCandidates)
		item.set_manage_text("Hire")
		item.connect("manage_button_pressed", self, "_on_ButtonHire_Pressed", [item])
	
	$ContainerSelect/HBoxContainer/Candidates/ContainerCandidates.update()

func _make_employee_item(employee, container):
	var item = EmployeeItem.instance()
	container.add_child(item)
	
	var job = ""
	if employee.CAPACITY_SELL in employee.capacities:
		job = "Seller"
	elif employee.CAPACITY_BUY in employee.capacities:
		job = "Buyer"
	
	item.set_employee(employee, null, null, job)
	
	return item

func _add_employee(employee):
	var item = _make_employee_item(employee, 
								   $ContainerSelect/HBoxContainer/Employees/ContainerEmployees)
	item.set_manage_text("Fire")
	item.connect("manage_button_pressed", self, "_on_ButtonFire_Pressed", [item])
	return item

func _on_ButtonFire_Pressed(item):
	game.fire_employee(item.employee)
	$ContainerSelect/HBoxContainer/Employees/ContainerEmployees.remove_child(item)
	$ContainerSelect/HBoxContainer/Employees/ContainerEmployees.update()

func _on_ButtonHire_Pressed(item):
	game.hire_candidate(item.employee)
	var new_item = _add_employee(item.employee)
	new_item.disable_manage_button()
	$ContainerSelect/HBoxContainer/Employees/ContainerEmployees.update()
	
	$ContainerSelect/HBoxContainer/Candidates/ContainerCandidates.remove_child(item)
	$ContainerSelect/HBoxContainer/Candidates/ContainerCandidates.update()
