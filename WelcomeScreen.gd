extends MarginContainer

const Game = preload("res://Game.gd")

signal new_game
signal load_game

func _ready():
	$VBoxContainer/ButtonResumeGame.disabled = !Game.has_saved_game()

func _on_ButtonNewGame_pressed():
	emit_signal("new_game")

func _on_ButtonResumeGame_pressed():
	emit_signal("load_game")
