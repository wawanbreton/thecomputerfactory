extends "AvailablePartItem.gd"

var required = 0 setget set_required

signal selected(model)


func set_part_in_stock(new_part_in_stock):
	.set_part_in_stock(new_part_in_stock)
	_update_available()

func set_required(new_required):
	required = new_required
	_update_available()
	
func _update_available():
	._update_available()
	if part_in_stock and part_in_stock.count < required:
		var label = $MarginContainer/HBoxContainer/VBoxContainer/LabelCount
		label.add_color_override("font_color", Color(1, 0, 0))
		$MarginContainer/HBoxContainer/Button.disabled = true

func _on_Button_pressed():
	if part_in_stock != null:
		emit_signal("selected", part_in_stock.model)
	else:
		emit_signal("selected", null)
