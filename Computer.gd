extends Node

const PartModel = preload("res://ComputerPartModel.gd")

var parts = []
var computing = 0
var graphics = 0
var ergonomy = 0

func _init():
	._init()
	_recompute_specs()

func get_spec(spec) -> int:
	match spec:
		PartModel.Spec.COMPUTING:
			return computing
		PartModel.Spec.GRAPHICS:
			return graphics
		PartModel.Spec.ERGONOMY:
			return ergonomy
	
	printerr("Unknown spec %d" % spec)
	return 0

func set_part(model):
	_clear_type(model.type)
	parts.append(model)
	_recompute_specs()

func clear_type(type):
	_clear_type(type)
	_recompute_specs()

func _clear_type(type):
	for part in parts:
		if part.type == type:
			parts.erase(part)
			break

func is_sellable():
	var all_required_parts = PartModel.Type.values()
	all_required_parts.erase(PartModel.Type.GRAPHICBOARD)
	
	for part in parts:
		print(part.type)
		all_required_parts.erase(part.type)
	
	return all_required_parts.empty()

func _recompute_specs():
	for spec in PartModel.Spec.values():
		var performance = 0
		
		for part in parts:
			performance += part.get_spec(spec)
		
		match spec:
			PartModel.Spec.COMPUTING:
				computing = performance
			PartModel.Spec.GRAPHICS:
				graphics = performance
			PartModel.Spec.ERGONOMY:
				ergonomy = performance
