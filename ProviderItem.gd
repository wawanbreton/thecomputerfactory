extends MarginContainer

var provider = null setget set_provider

var selected = false setget set_selected

func _ready():
	pass

func set_provider(new_provider):
	provider = new_provider
	$MarginContainer/HBoxContainer/VBoxContainer/LabelName.text = provider.name

func set_selected(new_selected):
	selected = new_selected
	$RectSelected.visible = selected
