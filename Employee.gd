extends "Character.gd"

enum {CAPACITY_BUY, CAPACITY_SELL, CAPACITY_MANAGER}
var capacities = []

var level = 1
var experience = 0

var daily_buys = 0
signal daily_buys_changed

var daily_sells = 0
signal daily_sells_changed


func properties_to_save():
	return .properties_to_save() + ["capacities", "level", "experience", "daily_buys", "daily_sells"]

func _ready():
	pass

func take_daily_buy():
	daily_buys -= 1
	emit_signal("daily_buys_changed")

func take_daily_sell():
	daily_sells -= 1
	emit_signal("daily_sells_changed")
