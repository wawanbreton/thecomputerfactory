extends "AvailablePartItem.gd"

onready var game = get_node("/root/Main/Game")


func set_part_in_stock(new_part_in_stock):
	.set_part_in_stock(new_part_in_stock)
	
	$MarginContainer/HBoxContainer/VBoxContainer/LabelPrice.text = game.get_money_label(part_in_stock.price)
	_update_available()
	_update_buy_button()
	
	part_in_stock.connect("count_changed", self, "_update_available")
	part_in_stock.connect("count_changed", self, "_update_buy_button")
	game.connect("money_changed", self, "_update_buy_button")

func _update_buy_button():
	var enabled = part_in_stock.count > 0 && part_in_stock.price < game.money
	$MarginContainer/HBoxContainer/Button.disabled = !enabled

func _on_Button_pressed():
	game.add_to_stock(part_in_stock)
