extends "SavableNode.gd"

var first_name = ""
var last_name = ""
var color_hair = null
var color_clothe = null
var color_skin = null

enum Gender {WOMAN, MAN}
var gender = Gender.WOMAN


func properties_to_save():
	return .properties_to_save() + ["first_name", 
									"last_name", 
									"gender",
									"color_hair",
									"color_clothe",
									"color_skin"]

func full_name():
	return "%s %s" % [first_name, last_name]
