extends "EmployeeItem.gd"

signal manage_button_pressed


func set_manage_text(text):
	$MarginContainer/HBoxContainer/ButtonManage.text = text

func disable_manage_button():
	$MarginContainer/HBoxContainer/ButtonManage.disabled = true

func _on_ButtonManage_pressed():
	emit_signal("manage_button_pressed")