extends Control

onready var game = get_node("/root/Main/Game")


func _ready():
	var balances = min(8, len(game.money_transfers))
	
	var days = []
	for i in range(balances):
		days.append(game.day - balances + i + 1)
	
	$CenterContainer/Grid.columns = 1 + balances
	
	$CenterContainer/Grid.add_child(Control.new())
	_add_days(days)
	
	_add_label("Cases:")
	_add_deltas(days, game.MoneyTransfer.BUY_CASE)
	
	_add_label("Motherboards:")
	_add_deltas(days, game.MoneyTransfer.BUY_MOTHERBOARD)
	
	_add_label("CPUs:")
	_add_deltas(days, game.MoneyTransfer.BUY_CPU)
	
	_add_label("Graphic boards:")
	_add_deltas(days, game.MoneyTransfer.BUY_GRAPHICBOARD)
	
	_add_label("Screens:")
	_add_deltas(days, game.MoneyTransfer.BUY_SCREEN)
	
	_add_label("Keyboards:")
	_add_deltas(days, game.MoneyTransfer.BUY_KEYBOARD)
	
	_add_spacers()
	
	_add_label("Buyers:")
	_add_deltas(days, game.MoneyTransfer.SALARY_BUYER)
	
	_add_label("Sellers:")
	_add_deltas(days, game.MoneyTransfer.SALARY_SELLER)
	
	_add_label("Severances:")
	_add_deltas(days, game.MoneyTransfer.SEVERANCE)
	
	_add_spacers()
	
	_add_label("Sold computers:")
	_add_deltas(days, game.MoneyTransfer.SELL_COMPUTER)

	_add_spacers()

	_add_label("Total:")
	_add_totals(days)

func _add_deltas(days, type):
	for day in days:
		_add_delta(game.money_transfers[day - 1][type])

func _add_delta(delta):
	var label = _add_label()
	
	if delta > 0:
		label.text = '+%d' % delta
		label.add_color_override("font_color", Color(0, 1, 0))
	elif delta < 0:
		label.text = '%d' % delta
		label.add_color_override("font_color", Color(1, 0, 0))
	else:
		label.text = '0'
		label.add_color_override("font_color", Color(1, 1, 1))

func _add_spacer():
	var rect = ColorRect.new()
	
	rect.rect_min_size.x = 60
	rect.rect_min_size.y = 2
	rect.rect_size.y = 2
	
	$CenterContainer/Grid.add_child(rect)

func _add_spacers():
#warning-ignore:unused_variable
	for i in range($CenterContainer/Grid.columns):
		_add_spacer()

func _add_total(day):
	var total = 0
	for transfer in game.MoneyTransfer.values():
		total += game.money_transfers[day - 1][transfer]
	_add_delta(total)

func _add_totals(days):
	for day in days:
		_add_total(day)

func _add_days(days):
	for day in days:
		_add_label("Day %d" % day)

func _add_label(text = ""):
	var label = Label.new()
	label.text = text
	label.align = HALIGN_RIGHT
	$CenterContainer/Grid.add_child(label)
	return label
