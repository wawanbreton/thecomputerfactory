extends Control

const BuyerItem = preload("res://EmployeeItem.tscn")
const ProviderItem = preload("res://ProviderItem.tscn")
const AvailablePartItem = preload("res://AvailablePartItemBuy.tscn")
const Utils = preload("res://Utils.gd")

onready var _providers = $ContainerSelect/HBoxContainer/Providers
onready var _providers_container = _providers.get_node("ContainerProviders")
onready var _buyers = $ContainerSelect/HBoxContainer/Buyers
onready var _buyers_container = _buyers.get_node("ContainerBuyers")


func _ready():
	for employee in get_node("/root/Main/Game/Employees").get_children():
		if employee.CAPACITY_BUY in employee.capacities:
			var buyer_item = BuyerItem.instance()
			_buyers_container.add_child(buyer_item)
			buyer_item.set_employee(employee, "daily_buys", "daily_buys_changed")

	_buyers_container.update()
	
	for provider in get_node("/root/Main/Game/DailyProviders").get_children():
		var provider_item = ProviderItem.instance()
		_providers_container.add_child(provider_item)
		provider_item.provider = provider
	
	_providers_container.update()
	
	_set_mode(true)

func _on_selection_changed():
	var selected_buyer_item = _buyers.get_selected_item()
	var selected_provider_item = _providers.get_selected_item()
	
	var callEnable = false
	
	if selected_buyer_item and selected_provider_item:
		callEnable = selected_buyer_item.employee.daily_buys > 0
	
	$ContainerSelect/ButtonCall.disabled = !callEnable

func _on_ButtonCall_pressed():
	_set_mode(false)
	
	var selected_buyer_item = _buyers.get_selected_item()
	_buyers.clear_selection()
	var buyer = selected_buyer_item.employee
	buyer.take_daily_buy()
	
	var selected_provider_item = _providers.get_selected_item()
	_providers.clear_selection()
	var provider = selected_provider_item.provider
	
	$ContainerBuy/Label.text = "Hi %s, here is my stock for today :" % buyer.first_name
	
	var container = $ContainerBuy/ScrollContainer/AvailableParts
	
	# Remove old displayed parts
	Utils.clear_children(container)
		
	# And add new ones
	for part_in_stock in provider.get_node("DailyStock").get_children():
		var item = AvailablePartItem.instance()
		container.add_child(item)
		item.part_in_stock = part_in_stock
		
func _set_mode(select):
	$ContainerSelect.visible = select
	$ContainerBuy.visible = !select

func _on_ButtonEndCall_pressed():
	_set_mode(true)
