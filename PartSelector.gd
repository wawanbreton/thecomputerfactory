extends Control

const Model = preload("res://ComputerPartModel.gd")

var type = null setget set_type
var required = 0
var model = null
var mandatory = true

signal model_changed

func set_type(new_type):
	type = new_type
	$MainContainer/LabelType.text = Model.type_str(type)

func clear():
	_set_model(null)

func _on_ButtonSelect_pressed():
	_select_part()

func _on_DialogSelect_selected(new_model):
	_set_model(new_model)
	
func _set_model(new_model):
	model = new_model
	$MainContainer/Control/ButtonSelect.visible = model == null
	$MainContainer/Control/TextureRect.visible = model != null
	if model != null:
		$MainContainer/Control/TextureRect.texture = model.image
	emit_signal("model_changed")

func _on_TextureRect_gui_input(event):
	if (event is InputEventMouseButton and
	    event.pressed and
	    event.button_index == BUTTON_LEFT and
		$MainContainer/Control/TextureRect.is_visible_in_tree()):
		_select_part()

func _select_part():
	var dialog = preload("res://DialogSelectPart.tscn").instance()
	add_child(dialog)
	dialog.type = type
	dialog.required = required
	dialog.display_none = !mandatory
	dialog.set_selected_part_model(model)
	dialog.popup_centered()
	dialog.connect("model_selected", self, "_on_DialogSelect_selected")