import cv2
import numpy
import os
import sys

sToken = sys.argv[1]

vtSourceImages = [('/tmp/main/Image0001.png', '%s.png' % sToken),
                               ('/tmp/mask_clothe/Image0001.png', 'mask_%s_clothe.png' % sToken),
                               ('/tmp/mask_hair/Image0001.png', 'mask_%s_hair.png' % sToken),
                               ('/tmp/mask_skin/Image0001.png', 'mask_%s_skin.png' % sToken)]

oMainImage = cv2.imread(vtSourceImages[0][0], cv2.IMREAD_UNCHANGED)
cv2.imwrite(vtSourceImages[0][1], oMainImage)
oAlphaChannel = cv2.split(oMainImage)[3]
oImageMaskLesser = cv2.compare(oAlphaChannel, 255, cv2.CMP_LT)
oImageMaskGreater = cv2.compare(oAlphaChannel, 0, cv2.CMP_GT)
oFillableArea = oImageMaskGreater & oImageMaskLesser

for sSrcFilePath, sDstFilePath in vtSourceImages[1:]:
    oMask = 255 - cv2.imread(sSrcFilePath, cv2.IMREAD_GRAYSCALE)
    oKernel = numpy.ones((3, 3), numpy.uint8)
    oDilated = cv2.dilate(oMask, oKernel)
    oAdded = oDilated ^ oMask
    oFinalMask = oMask | (oAdded & oFillableArea)
    cv2.imwrite(sDstFilePath, 255 - oFinalMask)
