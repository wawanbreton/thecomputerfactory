extends "SavableNode.gd"

const ComputerPartInStock = preload("res://ComputerPartInStock.tscn")

export(Texture) var logo = null


func set_daily_stock(parts_in_stock):
	Utils.clear_children($DailyStock)

	for part_in_stock in parts_in_stock:
		$DailyStock.add_child(part_in_stock)

func children_to_save():
	return .children_to_save() + $DailyStock.get_children()
