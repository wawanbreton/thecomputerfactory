extends MarginContainer

var selected = false setget set_selected
var employee = null
var _calls_property_name = ""


func set_employee(new_employee, 
				  calls_property_name, 
				  calls_changed_signal, 
				  description_text=null):
	employee = new_employee
	$MarginContainer/HBoxContainer/VBoxContainer/LabelName.text = employee.full_name()
	$MarginContainer/HBoxContainer/VBoxContainer/LabelLevel.text = "Level %d" % employee.level
	$MarginContainer/HBoxContainer/EmployeePortrait.set_colors(employee.color_hair, 
															   employee.color_clothe,
															   employee.color_skin,
															   employee.gender)

	if description_text != null:
		$MarginContainer/HBoxContainer/VBoxContainer/LabelDescription.text = description_text
	else:
		_calls_property_name = calls_property_name
		_update_calls()
		employee.connect(calls_changed_signal, self, "_update_calls")

func set_selected(new_selected):
	selected = new_selected
	$RectSelected.visible = selected

func _update_calls():
	$MarginContainer/HBoxContainer/VBoxContainer/LabelDescription.text = "%d call(s)" % employee.get(_calls_property_name)
