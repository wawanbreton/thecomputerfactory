extends Control

const Computer = preload("res://Computer.gd")
const Customer = preload("res://Customer.gd")
const Model = preload("res://ComputerPartModel.gd")

onready var game = get_node("/root/Main/Game")

var customer = null setget set_customer
var seller = null setget set_seller
var computer = Computer.new()
var _selectors = {}
var _price = 0

signal exit


func _ready():
	_selectors[Model.Type.CASE] = $HBoxContainer/Control/PartSelectorCase
	_selectors[Model.Type.MOTHERBOARD] = $HBoxContainer/Control/PartSelectorMotherBoard
	_selectors[Model.Type.CPU] = $HBoxContainer/Control/PartSelectorProcessor
	_selectors[Model.Type.GRAPHICBOARD] = $HBoxContainer/Control/PartSelectorGraphicBoard
	_selectors[Model.Type.SCREEN] = $HBoxContainer/Control/PartSelectorScreen
	_selectors[Model.Type.KEYBOARD] = $HBoxContainer/Control/PartSelectorAccessories
	
	for type in _selectors:
		var selector = _selectors[type]
		selector.type = type
		selector.connect("model_changed", self, "_on_Selector_model_changed", [selector])

	_selectors[Model.Type.GRAPHICBOARD].mandatory = false

	$HBoxContainer/Panel/ContainerCustomer/GridContainer/SliderComputing.max_value = game.max_possible_computer_specs[Model.Spec.COMPUTING]
	$HBoxContainer/Control/VBoxContainer/Panel2/GridContainer/SliderComputing.max_value = game.max_possible_computer_specs[Model.Spec.COMPUTING]
	$HBoxContainer/Panel/ContainerCustomer/GridContainer/SliderGraphics.max_value = game.max_possible_computer_specs[Model.Spec.GRAPHICS]
	$HBoxContainer/Control/VBoxContainer/Panel2/GridContainer/SliderGraphics.max_value = game.max_possible_computer_specs[Model.Spec.GRAPHICS]
	$HBoxContainer/Panel/ContainerCustomer/GridContainer/SliderErgonomy.max_value = game.max_possible_computer_specs[Model.Spec.ERGONOMY]
	$HBoxContainer/Control/VBoxContainer/Panel2/GridContainer/SliderErgonomy.max_value = game.max_possible_computer_specs[Model.Spec.ERGONOMY]

	_update_computer_specs()

func set_seller(new_seller):
	if seller != null:
		seller.disconnect("daily_sells_changed", self, "_update_computer_specs")
	
	seller = new_seller
	
	$HBoxContainer/Panel/ContainerCustomer/SellerItem.set_employee(seller, "daily_sells", "daily_sells_changed")
	seller.connect("daily_sells_changed", self, "_update_computer_specs")

func set_customer(new_customer):
	customer = new_customer
	$HBoxContainer/Panel/ContainerCustomer/CustomerItem.customer = customer
	
	var textNeed = "Computers needed: %d" % customer.nb_computers
	$HBoxContainer/Panel/ContainerCustomer/LabelNeedCount.text = textNeed
	
	var textBudget = "Budget: %s" % game.get_money_label(customer.budget)
	$HBoxContainer/Panel/ContainerCustomer/LabelBudget.text = textBudget
	
	for type in _selectors:
		_selectors[type].required = customer.nb_computers
	
	_price = customer.budget
	_update_price_label()
	
	for expected_spec in customer.expected_specs:
		var expected_value = customer.expected_specs[expected_spec]
		var text = ""
		var value = 0
		
		match expected_value[0]:
			Customer.SpecMatch.NONE:
				text = "None"
			Customer.SpecMatch.EXACT:
				value = expected_value[1]
				text = "%d" % value
			Customer.SpecMatch.ATLEAST:
				value = expected_value[1]
				text = "%d or more" % value
		
		match expected_spec:
			Model.Spec.GRAPHICS:
				$HBoxContainer/Panel/ContainerCustomer/GridContainer/LabelGraphicsValue.text = text
				$HBoxContainer/Panel/ContainerCustomer/GridContainer/SliderGraphics.value = value
			Model.Spec.COMPUTING:
				$HBoxContainer/Panel/ContainerCustomer/GridContainer/LabelComputingValue.text = text
				$HBoxContainer/Panel/ContainerCustomer/GridContainer/SliderComputing.value = value
			Model.Spec.ERGONOMY:
				$HBoxContainer/Panel/ContainerCustomer/GridContainer/LabelErgonomyValue.text = text
				$HBoxContainer/Panel/ContainerCustomer/GridContainer/SliderErgonomy.value = value

func clear_computer():
	for type in _selectors:
		_selectors[type].clear()

func _on_ButtonCancel_pressed():
	emit_signal("exit")

func _on_Selector_model_changed(selector):
	if selector.model != null:
		computer.set_part(selector.model)
	else:
		computer.clear_type(selector.type)
		
	_update_computer_specs()

func _update_computer_specs():
	var computing = computer.computing
	$HBoxContainer/Control/VBoxContainer/Panel2/GridContainer/SliderComputing.value = computing
	$HBoxContainer/Control/VBoxContainer/Panel2/GridContainer/LabelComputingValue.text = "%d" % computing
	
	var graphics = computer.graphics
	$HBoxContainer/Control/VBoxContainer/Panel2/GridContainer/SliderGraphics.value = graphics
	$HBoxContainer/Control/VBoxContainer/Panel2/GridContainer/LabelGraphicsValue.text = "%d" % graphics
	
	var ergonomy = computer.ergonomy
	$HBoxContainer/Control/VBoxContainer/Panel2/GridContainer/SliderErgonomy.value = ergonomy
	$HBoxContainer/Control/VBoxContainer/Panel2/GridContainer/LabelErgonomyValue.text = "%d" % ergonomy
	
	$HBoxContainer/Control/VBoxContainer/ButtonSubmit.disabled = (!computer.is_sellable() || 
																  seller == null || 
																  seller.daily_sells <= 0)

func _update_price_label():
	$HBoxContainer/Control/VBoxContainer/HBoxContainer/LabelPriceValue.text = game.get_money_label(_price)

func _on_ButtonPriceLess_pressed():
	_price = max(_price - 10, 0)
	_update_price_label()

func _on_ButtonPriceMore_pressed():
	_price += 10
	_update_price_label()

func _on_DialogAccepted_confirmed():
	emit_signal("exit")

func _on_ButtonSubmit_pressed():
	var dialog = AcceptDialog.new()
	var accepted = game.evaluate_customer_offer(customer, seller, computer, _price)
	
	if !accepted[0]:
		dialog.dialog_text = "Hmm, this is not what I need"
	elif !accepted[1]:
		dialog.dialog_text = "Hey, this is too expensive !"
	else:
		dialog.dialog_text = "Great, I take it"
		dialog.connect("confirmed", self, "_on_DialogAccepted_confirmed")
	
	add_child(dialog)
	dialog.popup_centered()
