extends "SavableNode.gd"

var model = null
var price = 0
var count = 0 setget set_count
signal count_changed


func properties_to_save():
	return .properties_to_save() + ["model", "price", "count"]

func set_count(new_count):
	if new_count != count:
		count = new_count
		emit_signal("count_changed")
