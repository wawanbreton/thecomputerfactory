extends PopupDialog

const PartItem = preload("res://AvailablePartItemSelect.tscn")

var type = null setget set_type
var required = null setget set_required
var display_none = null setget set_display_none

signal model_selected(model)


func set_type(new_type):
	type = new_type
	prepare_parts()

func set_required(new_required):
	required = new_required
	prepare_parts()

func set_display_none(new_display_none):
	display_none = new_display_none
	prepare_parts()

func set_selected_part_model(selected_part_model):
	for part_item in $MarginContainer/VBoxContainer/ScrollContainer/Parts.get_children():
		part_item.selected = ((part_item.part_in_stock == null && selected_part_model == null) ||
							  (part_item.part_in_stock != null && part_item.part_in_stock.model == selected_part_model))

func prepare_parts():
	if type != null and required != null and display_none != null:
		var displayed_parts = get_node("/root/Main/Game/Stock").get_children()
		
		if display_none:
			displayed_parts.push_front(null)
		
		for part in displayed_parts:
			if part == null or part.model.type == type:
				var item = PartItem.instance()
				$MarginContainer/VBoxContainer/ScrollContainer/Parts.add_child(item)
				item.part_in_stock = part
				item.required = required
				item.size_flags_horizontal |= SIZE_EXPAND
				item.connect("selected", self, "_on_PartItem_selected")

func _on_ButtonCancel_pressed():
	get_parent().remove_child(self)

func _on_PartItem_selected(model):
	emit_signal("model_selected", model)
	get_parent().remove_child(self)
