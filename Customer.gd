extends "Character.gd"

enum Profile {GAMER, 
			  LAN_GAMING_CENTER, 
			  PRIVATE,
			  COMPANY,
			  DEVELOPPER,
			  CONSULTING_COMPANY,
			  DESIGNER,
			  DESIGN_STUDIO}

enum SpecMatch {NONE,    # Customer doesn't care about this spec
				EXACT,   # At least this value, but more is not required
				ATLEAST} # At least this value, but more is better

var profile = null
var nb_computers = 0
var expected_specs = {}
var budget = 0
var specs_score_tolerancy = 0.0
var budget_score_tolerancy = 0.0


func properties_to_save():
	return (.properties_to_save() + [
			"profile",
			"nb_computers",
			"expected_specs",
			"budget",
			"specs_score_tolerancy",
			"budget_score_tolerancy"
			])

func profile_str():
	match profile:
		Profile.GAMER:
			return "Gamer"
		Profile.LAN_GAMING_CENTER:
			return "LAN gaming center"
		Profile.PRIVATE:
			return "Private"
		Profile.COMPANY:
			return "Company"
		Profile.DEVELOPPER:
			return "Developper"
		Profile.CONSULTING_COMPANY:
			return "Consulting company"
		Profile.DESIGNER:
			return "Designer"
		Profile.DESIGN_STUDIO:
			return "Design studio"
