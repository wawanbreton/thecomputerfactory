extends ScrollContainer

var _mouse_press_position = null
var _dragging = false
var _scroll_start_h = 0
var _scroll_start_v = 0
const _threshold_move = 5

func _ready():
	get_tree().connect("node_added", self, "_on_node_added")

func _on_node_added(node):
	if is_a_parent_of(node):
		if !node is Button and node.mouse_filter == MOUSE_FILTER_STOP:
			node.mouse_filter = MOUSE_FILTER_PASS

func _gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.pressed:
				_mouse_press_position = event.position
			else:
				if !_dragging:
					_mouse_clicked(_mouse_press_position)
				_mouse_press_position = null
				
			_dragging = false
	
	elif event is InputEventMouseMotion:
		if _mouse_press_position != null:
			if _dragging:
				scroll_vertical = _scroll_start_v + (_mouse_press_position.y - event.position.y)
				scroll_horizontal = _scroll_start_h + (_mouse_press_position.x - event.position.x)
			elif event.position.distance_to(_mouse_press_position) >= _threshold_move:
				_scroll_start_h = scroll_horizontal
				_scroll_start_v = scroll_vertical
				_dragging = true
	
func _mouse_clicked(position):
	# To be overridden by child classes
	pass