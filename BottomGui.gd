extends HBoxContainer

onready var game = get_node("/root/Main/Game")

func _ready():
	update_money()
	game.connect("money_changed", self, "update_money")
	
	update_day()
	game.connect("day_changed", self, "update_day")
	
	update_step()
	game.connect("step_changed", self, "update_step")

func update_money():
	$MoneyRect/LabelMoney.text = game.get_money_label()

func update_day():
	$DayRect/LabelDay.text = 'Day %d' % game.day

func update_step():
	var mapLabels = {$LabelBuys:      game.Step.BUYS, 
					 $LabelSell:      game.Step.SELL,
					 $LabelEmployees: game.Step.EMPLOYEES,
					 $LabelBalance:   game.Step.BALANCE}
	
	for label in mapLabels:
		if game.step == mapLabels[label]:
			label.add_color_override("font_color", Color(1, 0, 0))
		else:
			label.add_color_override("font_color", Color(1, 1, 1))

func _on_ButtonNextStep_pressed():
	game.next_step()
