extends Node

static func round(value, precision):
	return int(round(float(value) / precision)) * precision

static func merge(dict1 : Dictionary, dict2 : Dictionary) ->  Dictionary:
	var dict = dict1.duplicate()
	
	for key in dict2:
		dict[key] = dict2[key]
	
	return dict

static func clear_children(node : Node) -> void:
	for child in node.get_children():
		node.remove_child(child)

static func clear_random(node : Node, dest_size : int) -> void:
	while node.get_child_count() > dest_size:
		node.remove_child(node.get_child(randi() % node.get_child_count()))

static func get_random(array : Array) -> Object:
	return array[randi() % len(array)]
