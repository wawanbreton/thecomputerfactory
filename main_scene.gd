extends Node

var current_step_gui = null

onready var game = get_node("/root/Game")

func _ready():
	update_step()
	game.connect("step_changed", self, "update_step")

func update_step():
	if current_step_gui:
		$VBoxContainer/StepGuiContainer.remove_child(current_step_gui)
		current_step_gui = null
	
	match game.step:
		game.Step.BUYS:
			current_step_gui = preload("res://GuiStepBuy.tscn").instance()
		game.Step.SELL:
			current_step_gui = preload("res://GuiStepSell.tscn").instance()
		game.Step.EMPLOYEES:
			current_step_gui = preload("res://GuiStepEmployees.tscn").instance()
		game.Step.BALANCE:
			current_step_gui = preload("res://GuiStepBalance.tscn").instance()
	
	$VBoxContainer/StepGuiContainer.add_child(current_step_gui)
