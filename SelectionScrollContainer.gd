extends "TouchScrollContainer.gd"

signal selection_changed


func get_selected_item():
	for item in get_children().front().get_children():
		if item.selected:
			return item
	
	return null

func _mouse_clicked(position):
	var container = get_children().front()
	if container.get_rect().has_point(position):
		accept_event()
		var local_mouse_pos = container.get_local_mouse_position()
		
		for item in container.get_children():
			if item.get_rect().has_point(local_mouse_pos):
				_set_selected_item(item)
				break

func clear_selection():
	_set_selected_item(null)

func _set_selected_item(item_to_select):
	var container = get_children().front()
	var old_selected_item = null
	
	for item in container.get_children():
		if item.selected:
			old_selected_item = item
			break

	if item_to_select != old_selected_item:
		if old_selected_item:
			old_selected_item.selected = false
			
		if item_to_select:
			item_to_select.selected = true
		
		emit_signal("selection_changed")
