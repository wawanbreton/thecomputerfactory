extends MarginContainer

var selected = false setget set_selected
var customer = null setget set_customer

func set_customer(new_customer):
	customer = new_customer
	$MarginContainer/HBoxContainer/VBoxContainer/LabelName.text = customer.full_name()
	$MarginContainer/HBoxContainer/VBoxContainer/LabelProfile.text = customer.profile_str()
	$MarginContainer/HBoxContainer/CharacterPortrait.set_colors(customer.color_hair, 
																customer.color_clothe,
																customer.color_skin,
																customer.gender)

func set_selected(new_selected):
	selected = new_selected
	$RectSelected.visible = selected
