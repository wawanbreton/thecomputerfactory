extends Control

const SellerItem = preload("res://EmployeeItem.tscn")
const CustomerItem = preload("res://CustomerItem.tscn")

onready var _sellers = $ContainerSelect/HBoxContainer/Sellers
onready var _sellers_container = _sellers.get_node("ContainerSellers")
onready var _customers = $ContainerSelect/HBoxContainer/Customers
onready var _customers_container = _customers.get_node("ContainerCustomers")


func _ready():
	for employee in get_node("/root/Main/Game/Employees").get_children():
		if employee.CAPACITY_SELL in employee.capacities:
			var seller_item = SellerItem.instance()
			_sellers_container.add_child(seller_item)
			seller_item.set_employee(employee, "daily_sells", "daily_sells_changed")

	_sellers_container.update()

	for customer in get_node("/root/Main/Game/DailyCustomers").get_children():
		var customer_item = CustomerItem.instance()
		_customers_container.add_child(customer_item)
		customer_item.customer = customer
		customer.connect("tree_exited", self, "_on_Customer_exited", [customer])
	
	_customers_container.update()
	
	_set_mode(true)

func _on_selection_changed():
	var selected_seller_item = _sellers.get_selected_item()
	var selected_customer_item = _customers.get_selected_item()
	
	var callEnable = false
	
	if selected_seller_item and selected_customer_item:
		callEnable = selected_seller_item.employee.daily_sells > 0
	
	$ContainerSelect/ButtonCall.disabled = !callEnable

func _set_mode(select):
	$ContainerSelect.visible = select
	$GuiCustomerOffer.visible = !select

func _on_ButtonCall_pressed():
	$GuiCustomerOffer.clear_computer()
	_set_mode(false)
	
	var selected_seller_item = _sellers.get_selected_item()
	_sellers.clear_selection()
	var seller = selected_seller_item.employee
	seller.take_daily_sell()
	
	var selected_customer_item = _customers.get_selected_item()
	_customers.clear_selection()
	var customer = selected_customer_item.customer
	
	$GuiCustomerOffer.seller = seller
	$GuiCustomerOffer.customer = customer

func _on_GuiCustomerOffer_exit():
	_set_mode(true)

func _on_Customer_exited(customer):
	for customer_item in _customers_container.get_children():
		if customer_item.customer == customer:
			_customers_container.remove_child(customer_item)
			break
