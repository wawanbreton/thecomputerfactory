extends TextureRect

const Character = preload("res://Character.gd")


func set_colors(hair, clothe, skin, gender):
	material.set_shader_param("hair_color", hair)
	material.set_shader_param("clothe_color", clothe)
	material.set_shader_param("skin_color", skin)
	
	match gender:
		Character.Gender.MAN:
			material.set_shader_param("texture_main", preload("res://resources/man.png"))
			material.set_shader_param("texture_mask_clothe", preload("res://resources/mask_man_clothe.png"))
			material.set_shader_param("texture_mask_hair", preload("res://resources/mask_man_hair.png"))
			material.set_shader_param("texture_mask_skin", preload("res://resources/mask_man_skin.png"))
		Character.Gender.WOMAN:
			material.set_shader_param("texture_main", preload("res://resources/woman.png"))
			material.set_shader_param("texture_mask_clothe", preload("res://resources/mask_woman_clothe.png"))
			material.set_shader_param("texture_mask_hair", preload("res://resources/mask_woman_hair.png"))
			material.set_shader_param("texture_mask_skin", preload("res://resources/mask_woman_skin.png"))
