extends MarginContainer

var part_in_stock = null setget set_part_in_stock
var selected = false setget set_selected

func set_part_in_stock(new_part_in_stock):
	part_in_stock = new_part_in_stock
	
	if part_in_stock != null:
		var model = part_in_stock.model
		
		$MarginContainer/HBoxContainer/VBoxContainer/LabelName.text = model.name
		$MarginContainer/HBoxContainer/VBoxContainer/LabelType.text = model.type_str(model.type)
		$MarginContainer/HBoxContainer/GridContainer/SliderComputing.value = model.computing
		$MarginContainer/HBoxContainer/GridContainer/SliderComputing.hint_tooltip = "Computing: %d" % model.computing
		$MarginContainer/HBoxContainer/GridContainer/SliderGraphics.value = model.graphics
		$MarginContainer/HBoxContainer/GridContainer/SliderGraphics.hint_tooltip = "Graphics: %d" % model.graphics
		$MarginContainer/HBoxContainer/GridContainer/SliderErgonomy.value = model.ergonomy
		$MarginContainer/HBoxContainer/GridContainer/SliderErgonomy.hint_tooltip = "Ergonomy: %d" % model.ergonomy
		$MarginContainer/HBoxContainer/TextureRect.texture = model.image
	else:
		$MarginContainer/HBoxContainer/VBoxContainer/LabelName.text = "None"
		$MarginContainer/HBoxContainer/GridContainer/SliderComputing.value = 0
		$MarginContainer/HBoxContainer/GridContainer/SliderGraphics.value = 0
		$MarginContainer/HBoxContainer/GridContainer/SliderErgonomy.value = 0

func set_selected(new_selected):
	selected = new_selected
	$ColorRectSelected.visible = selected

func _update_available():
	if part_in_stock != null:
		var text = "%d available" % part_in_stock.count
		
		var player_stock = 0
		for part_in_player_stock in get_node("/root/Main/Game/Stock").get_children():
			if part_in_player_stock.model == part_in_stock.model:
				player_stock = part_in_player_stock.count
		
		if player_stock > 0:
			text += " (%d in stock)" % player_stock
		
		$MarginContainer/HBoxContainer/VBoxContainer/LabelCount.text = text
	else:
		$MarginContainer/HBoxContainer/VBoxContainer/LabelCount.text = ""

func _on_Button_pressed():
	# To be overridden
	pass
