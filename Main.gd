extends Control

const Game = preload("res://Game.gd")

onready var line_edit_manager_first_name = $GuiCompanyDetails/CenterContainer/VBoxContainer/GridContainer/LineEditManagerFirstName
onready var line_edit_manager_last_name = $GuiCompanyDetails/CenterContainer/VBoxContainer/GridContainer/LineEditManagerLastName


func _init():
	randomize()

func _ready():
	_make_random_first_name()
	_make_random_last_name()
	
	_set_visible(true, false)

func _set_visible(welcome, details):
	$WelcomeScreen.visible = welcome
	$GuiCompanyDetails.visible = details

func _on_WelcomeScreen_over():
	_set_visible(false, false)

func _add_game(game):
	game.name = "Game"
	add_child(game)

func _on_WelcomeScreen_load_game():
	_add_game(Game.load(get_node("/root")))
	_on_WelcomeScreen_over()

func _on_WelcomeScreen_new_game():
	_set_visible(false, true)
	
func _on_ButtonGo_pressed():
	var game = preload("res://Game.tscn").instance()
	_add_game(game)
	game.prepare_new(line_edit_manager_first_name.text, 
					 line_edit_manager_last_name.text)
	_on_WelcomeScreen_over()

func _make_random_first_name():
	line_edit_manager_first_name.text = Game.get_random_first_name()

func _make_random_last_name():
	line_edit_manager_last_name.text = Game.get_random_last_name()

func _on_ButtonRandomFirstName_pressed():
	_make_random_first_name()

func _on_ButtonRandomLastName_pressed():
	_make_random_last_name()
