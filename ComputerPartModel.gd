extends Node

const Utils = preload("res://Utils.gd")

enum Type {CASE, 
		   MOTHERBOARD, 
		   CPU, 
		   GRAPHICBOARD, 
		   SCREEN, 
		   KEYBOARD}
export(Type) var type

enum Spec {COMPUTING, GRAPHICS, ERGONOMY}
export(int, 0, 100) var graphics = 0
export(int, 0, 100) var computing = 0
export(int, 0, 100) var ergonomy = 0 

export(int) var min_cost = 0
export(int) var max_cost = 0

export(Texture) var image = null


func get_spec(spec) -> int:
	match spec:
		Spec.COMPUTING:
			return computing
		Spec.GRAPHICS:
			return graphics
		Spec.ERGONOMY:
			return ergonomy
	
	printerr("Unknown spec %d" % spec)
	return 0

#func save():
#	return Utils.merge(Utils.save_object(self), {
#		"type" : type,
#		"specs" : specs,
#		"min_cost" : min_cost,
#		"max_cost" : max_cost
#	})

static func type_str(type):
	match type:
		Type.CASE:
			return "Case"
		Type.MOTHERBOARD:
			return "Motherboard"
		Type.CPU:
			return "Processor"
		Type.GRAPHICBOARD:
			return "Graphic board"
		Type.SCREEN:
			return "Screen"
		Type.KEYBOARD:
			return "Keyboard"
